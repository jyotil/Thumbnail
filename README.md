# To get started do the following

- Clone the repo `git clone https://jyotil@gitlab.com/jyotil/Thumbnail.git`
- do `cd Thumbnail && bower install && npm install` (if bower is not installed, do `npm install bower -g`)
- do `gulp serve` (if gulp not installed, do `npm install gulp -g`
