(function() {
  'use strict';

  angular
  .module('web')
  .controller('FillTimeSlotController', FillTimeSlotController);

  FillTimeSlotController.$inject = ['timeServeice', '$state'];

  /** @ngInject */
  function FillTimeSlotController(timeServeice, $state) {
    var vm = this;

    vm.index = 0;
    vm.detail = {};

    vm.cancle = cancle;
    vm.save = save;

    activate();

    function save() {
      timeServeice.slot[vm.index].detail = vm.detail;
      go();
    }

    function go() {
      $state.go('bookTimeSlot');
    }

    function cancle() {
      vm.detail = timeServeice.slot[vm.index].detail;
      go();
    }


    function activate() {
      var detailId = $state.params.timeId;
      vm.index = timeServeice.slot.findIndex(function (time){
        return time.id == $state.params.timeId
      });
      vm.detail = angular.copy(timeServeice.slot[vm.index].detail);
    }
  }
})();
