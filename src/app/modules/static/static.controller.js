(function() {
  'use strict';

  angular
    .module('web')
    .controller('MainStaticController', MainStaticController);

  MainStaticController.$inject = ['$timeout'];

  /** @ngInject */
  function MainStaticController($timeout) {
    var vm = this;

    vm.Imagesss = [{
      'thumbUrl':'../assets/images/angular.png',
      'url':'../assets/images/angular.png'
    },{
      'thumbUrl':'../assets/images/bootstrap.png',
      'url':'../assets/images/bootstrap.png'
    },{
      'thumbUrl':'../assets/images/browsersync.png',
      'url':'../assets/images/browsersync.png'
    },{
      'thumbUrl':'../assets/images/gulp.png',
      'url':'../assets/images/gulp.png'
    },{
      'thumbUrl':'../assets/images/jasmine.png',
      'url':'../assets/images/jasmine.png'
    },{
      'thumbUrl':'../assets/images/karma.png',
      'url':'../assets/images/karma.png'
    },{
      'thumbUrl':'../assets/images/protractor.png',
      'url':'../assets/images/protractor.png'
    },{
      'thumbUrl':'../assets/images/ui-bootstrap.png',
      'url':'../assets/images/ui-bootstrap.png'
    },{
      'thumbUrl':'../assets/images/yeoman.png',
      'url':'../assets/images/yeoman.png'
    }];

    activate();

    function activate() {
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }
  }
})();
