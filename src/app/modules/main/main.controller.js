(function() {
  'use strict';

  angular
    .module('web')
    .controller('MainController', MainController);

  MainController.$inject = ['$timeout', 'toastr', 'Restangular'];

  /** @ngInject */
  function MainController($timeout, toastr, Restangular) {
    var vm = this;

    vm.Imagesss = [];

    activate();

    function getImages() {
      Restangular
      .one('api/?key=5046360-1b01f7d3ca7984c3e5ce8c7f8')
      .get()
      .then(function (response) {
        response.data.hits.forEach(function (hit) {
          toastr.info('All images are loaded', 'Information');
          vm.Imagesss.push({
            'thumbUrl':hit.previewURL,
            'url':hit.webformatURL
          })
        });
      })
      .catch(function (err) {
        console.log(err);
      });
    }

    function activate() {
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);

      getImages();
    }
  }
})();
