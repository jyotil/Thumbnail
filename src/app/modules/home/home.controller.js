(function() {
  'use strict';

  angular
    .module('web')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['$timeout', 'toastr', 'Restangular'];

  /** @ngInject */
  function HomeController($timeout, toastr, Restangular) {
    var vm = this;

    activate();

    function activate() {
      $timeout(function() {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }
  }
})();
