(function() {
  'use strict';

  angular
    .module('web')
    .controller('BookTimeSlotController', BookTimeSlotController);

  BookTimeSlotController.$inject = ['timeServeice'];

  /** @ngInject */
  function BookTimeSlotController(timeServeice) {
    var vm = this;

    vm.timeSlots = [];

    activate();

    function generateTimeSlot() {
      if (!timeServeice.slot.length) {
        for (var i = 9; i < 18; i++) {
          timeServeice.slot.push({
            'id': i+'',
            'slot': i+':00',
            'detail':{
              'fname':'',
              'lname':'',
              'contact':''
            }
          });
        }
      }
      vm.timeSlots = angular.copy(timeServeice.slot);
    }

    function activate() {
      generateTimeSlot()
    }
  }
})();
