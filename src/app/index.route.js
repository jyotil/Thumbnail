(function() {
  'use strict';

  angular
  .module('web')
  .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/modules/home/home.html',
      controller: 'HomeController',
      controllerAs: 'vm'
    })
    .state('bookTimeSlot', {
      url: '/book-time-slot',
      templateUrl: 'app/modules/bookTimeSlot/bookTimeSlot.html',
      controller: 'BookTimeSlotController',
      controllerAs: 'vm'
    })
    .state('fillTimeSlot', {
      url: '/fill-time-slot/{timeId}',
      params: {
        timeId: { value: null }
      },
      templateUrl: 'app/modules/fillTimeSlot/fillTimeSlot.html',
      controller: 'FillTimeSlotController',
      controllerAs: 'vm'
    })
    .state('dynamic', {
      url: '/dynamic',
      templateUrl: 'app/modules/main/main.html',
      controller: 'MainController',
      controllerAs: 'vm'
    })
    .state('static', {
      url: '/static',
      templateUrl: 'app/modules/main/main.html',
      controller: 'MainStaticController',
      controllerAs: 'vm'
    });

    $urlRouterProvider.otherwise('/book-time-slot');
  }

})();
