(function() {
  'use strict';

  angular
  .module('web')
  .filter('stringToTimeFilter', stringToTimeFilter);

  function stringToTimeFilter($filter) {
    return filterFilter

    function filterFilter(time, format) {
      var parts = time.split(':');
      var date = new Date(0, 0, 0, parts[0] || "00", parts[1] || "00", parts[2] || "00");
      return $filter('date')(date, format || 'HH:mm a');
    }
  }
})();
