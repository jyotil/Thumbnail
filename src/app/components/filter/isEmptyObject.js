(function() {
    'use strict';

    angular
        .module('web')
        .filter('isEmptyObject', isEmptyObject);

    function isEmptyObject() {
        return isEmptyObjectFilter;

        function isEmptyObjectFilter(obj) {
            return obj && Object.keys(obj).length === 0;
        }
    }
})();
