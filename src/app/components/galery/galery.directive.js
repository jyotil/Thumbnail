(function() {
  'use strict';

  angular
    .module('web')
    .directive('galery', galery);

  /** @ngInject */
  function galery() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/galery/galery.html',
      scope: {
          imagess: '='
      },
      controller: galeryController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function galeryController(Lightbox) {
      var vm = this;

      vm.openLightBox = openLightBox;
      function openLightBox(idx) {
        Lightbox.openModal(vm.imagess, idx);
      }

      // "vm.creationDate" is available by directive option "bindToController: true"
    }
  }

})();
